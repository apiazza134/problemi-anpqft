\documentclass[a4paper,11pt]{article}

\usepackage{style}

\title{Problems for NPQFT}
\author{Alessandro Piazza}

\hypersetup{
    pdfauthor={},
    pdftitle={},
    pdfkeywords={},
    pdfsubject={},
    pdfcreator={},
    pdflang={}
}

\begin{document}
\maketitle

\tableofcontents

\section{Lattice QCD: problem 3}
\begin{enumerate}[label=(\alph*)]
    \item Let \( \Theta = H / \sqrt{H^{\dagger} H} \), then
    \begin{align*}
      D_{ov}\gamma_{5} + \gamma_{5} D_{ov} - D_{ov} \gamma_{5} D_{ov}
      &= (1 + \gamma_{5} \Theta) \gamma_{5} + \gamma_{5}(1 + \gamma_{5} \Theta) - (1 + \gamma_{5} \Theta) \gamma_{5} (1 + \gamma_{5} \Theta) \\
      &= \gamma_{5} + \gamma_{5} \Theta \gamma_{5} + \gamma_{5} + \Theta - \gamma_{5} - \gamma_{5} \Theta \gamma_{5} - \Theta - \gamma_{5} \Theta^{2} \\
      &= \gamma_{5}(1 - \Theta^{2})
    \end{align*}
    where we have used multiple times that \( \gamma_{5}^{2} = 1 \). If \( H \) is an hermitian operator then
    \begin{equation}
        \Theta^{2} = \frac{1}{\sqrt{H^{\dagger} H}} H \frac{1}{\sqrt{H^{\dagger} H}} H = \frac{1}{\abs{H}} H \frac{1}{\abs{H}} H = \frac{1}{H^{2}} H^{2} = 1
    \end{equation}
    and therefore the Ginsparg-Wilson relation is satisfied.

    To prove that this is indeed the case let us write \( H = \gamma_{5} A \) where
    \begin{equation}
        A = D_{W} - M = \hat{D} - \frac{r}{2} \hat{\Box} - M
    \end{equation}
    where \( \hat{D} \) is the discretized Dirac operator and \( \hat{\Box} \) the discretized Laplacian. Now the Dirac operator is anti-hermitian, whereas the Laplacian and the identity are hermitian operators. \( A \) is then not hermitian but it's \( \gamma_{5} \)-hermitian
    \begin{equation}
        (A \gamma_{5})^{\dagger} = \gamma_{5} A^{\dagger} = \gamma_{5} \left(- \hat{D} - \frac{r}{2} \hat{\Box} - M\right) = \left(\hat{D} - \frac{r}{2} \hat{\Box} - M\right)\gamma_{5} = A \gamma_{5}
    \end{equation}
    where we have used that \( \gamma_{5} \) is hermitian and that \( \hat{D} \) is a linear combination of \( \gamma^{\mu} \)-s in the Dirac indexes hence anti-commutes with \( \gamma_{5} \), whereas the Laplacian and the identity are the identity in Dirac indexes, hence commutes with \( \gamma_{5} \).

    Using the \( \gamma_{5} \)-hermiticity of \( A \) we finally have
    \begin{equation}
        H^{\dagger} = (\gamma_{5} A)^{\dagger} = A^{\dagger} \gamma_{5} = \gamma_{5} A \gamma_{5} \gamma_{5} = \gamma_{5} A = H
    \end{equation}
    \item Le \( u_{0} \) be a zero-mode of \( D_{ov} \), then, using the Ginsparg-Wilson relation
    \begin{align*}
      [\gamma_{5}, D_{ov}] u_{0} &= \gamma_{5} D_{ov} u_{0} + D_{ov} \gamma_{5} u_{0} \\
                                 &= \left(- \gamma_{5} D_{ov} + D_{ov} \gamma_{5} D_{ov}\right) u_{0} = 0
    \end{align*}
    Since \( D_{ov} \) commutes with \( \gamma_{5} \) in the subspace of zero-modes we can find a basis of eigenstates of \( D_{ov} \) which are also eigenstates of \( \gamma_{5} \), i.e. having a well defined chirality
    \item Let \( \mathcal{D}(\chi, \psi) \) be the bi-linear operator corresponding to the Dirac operator
    \begin{equation}
        \mathcal{D}(\chi, \psi)
        = \int \dif[4]{x} \, \dif[4]{y} \, \bar{\chi}(x) \delta^{(4)}(x-y) \cancel{D}_{y} \psi(y)
        = \int \frac{\dif[4]{p}}{(2\pi)^{4}} \tilde{\bar{\chi}}(-p) \left(-i \cancel{p}\right) \tilde{\psi}(p)
    \end{equation}
    We wish to show that
    \begin{equation}
        \label{eq:continoum-limit}
        \mathcal{D}(\chi, \psi) = \lim_{a \to 0} \hat{\mathcal{D}}_{ov}(\hat{\chi}, \hat{\psi})
    \end{equation}
    where
    \begin{equation}
        \hat{\mathcal{D}}_{ov}(\hat{\chi}, \hat{\psi}) = \sum_{m,n} \hat{\bar\chi}_{m} (D_{ov})_{mn} \hat{\psi}_{n}
    \end{equation}
    where \( \hat{\psi}_{n} \) is the discretized spinor
    \begin{equation}
        \hat{\psi}_{n} = a^{3/2} \psi(a n)
    \end{equation}
    To prove~\eqref{eq:continoum-limit} we use the Fourier transform representation
    \begin{equation}
        \hat{\psi}_{k} = \sum_{n} e^{ikn} \hat{\psi}_{n} \qquad \hat{\psi}_{n} = \int_{-\pi}^{\pi} \frac{\dif[4]{k}}{(2\pi)^{4}} e^{-ikn} \hat{\psi}_{k}
    \end{equation}
    where we have dropped the tilde for sake of notation. The relation to the continuous Fourier transform is
    \begin{equation}
        \hat{\psi}_{k} = a^{3/2 - 4} \tilde{\psi}(k / a) = a^{-5/2} \tilde{\psi}(k / a)
    \end{equation}
    The overlap operator is invariant under translations, hence will have a Fourier transform written as
    \begin{equation}
        \sum_{nm} e^{ikn+iqm} (D_{ov})_{nm} = (2\pi)^{4} \delta^{(4)}(k + q) \tilde{D}_{k}
    \end{equation}
    Hence the overlap bi-linear will be
    \begin{equation}
        \hat{\mathcal{D}}_{ov}(\hat{\chi}, \hat{\psi}) = \int_{-\pi}^{\pi} \frac{\dif[4]{k}}{(2\pi)^{4}} \hat{\bar\chi}_{-k}  \tilde{D}_{k} \hat{\psi}_{k} = \int_{-\pi/a}^{\pi/a} \frac{\dif[4]{p}}{(2\pi)^{4}} \tilde{\bar\chi}(-p) \left(\frac{\hat{D}_{k = a p}}{a}\right) \tilde{\psi}(p)
    \end{equation}
    Provided that there are no additional problems (like fermion doubling), showing~\eqref{eq:continoum-limit} amounts to proving that
    \begin{equation}
        \lim_{a\to 0} \frac{\hat{D}_{a p}}{a} = - i \cancel{p}
    \end{equation}
    To prove this relation we first compute the Fourier transform of the \( A \) operator
    \begin{align*}
      \sum_{m,n} e^{imk+inq} A_{mn}
      &= \sum_{m,n} e^{imk+inq} \left[\frac{1}{2}\sum_{\mu=1}^{4} \gamma^{\mu}(\delta_{n,m+\hat{\mu}} - \delta_{n,m-\hat{\mu}}) - \frac{r}{2} \sum_{\mu=1}^{4}(\delta_{n,m+\hat{\mu}}-2 \delta_{n,m} + \delta_{n, m-\hat{\mu}}) - M \delta_{mn}\right] \\
      &= \frac{1}{2} \sum_{\mu=1}^{4} \gamma^{\mu} \sum_{n}e^{in(k+q)} (e^{i k_{\mu}} - e^{-i k_{\mu}}) - \frac{r}{2} \sum_{n} e^{in(k+q)}(e^{ik_{\mu}} + e^{-ik_{\mu}} - 2) - M \sum_{n} e^{in(k+q)} \\
      &= (2\pi)^{4} \delta^{(4)}(k+q)\left[i \sum_{\mu=1}^{4} \gamma^{\mu} \sin(k_{\mu}) - r \sum_{\mu=1}^{4}(\cos(k_{\mu})-1) - M\right]
    \end{align*}
    hence
    \begin{equation}
        \tilde{A}_{k} = i \sum_{\mu=1}^{4} \gamma^{\mu} \sin(k_{\mu}) + 2r \sum_{\mu=1}^{4}\sin^{2}(k_{\mu}/2) - M = i \gamma^{\mu} v_{\mu}(k) + v_{0}(k)
    \end{equation}
    Then we have
    \begin{align*}
      \widetilde{(A^{\dagger} A)}_{k} = \tilde{A}_{k}^{*} A_{k} = (-i \gamma^{\mu} v_{\mu} + v_{0}) ( i\gamma^{\mu} v_{\mu} + v_{0}) = \gamma^{\mu} \gamma^{\nu} v_{\mu} v_{\nu} + v_{0}^{2} = v^{2} + v_{0}^{2}
    \end{align*}
    Since the Fourier transform of \( A^{\dagger} A \) is the identity in the Dirac indexes we have
    \begin{equation}
        \gamma_{5} \widetilde{\left(H \frac{1}{\sqrt{H^{\dagger} H}}\right)}_{k} = \tilde{A}_{-k} \widetilde{\left(\frac{1}{\sqrt{A^{\dagger} A}}\right)}_{k} = \frac{- i \gamma^{\mu} v_{\mu}(k) + v_{0}(k)}{\sqrt{v^{2}(k) + v_{0}^{2}(k)}}
    \end{equation}
    since \( v_{\mu}(-k) = \sin(-k_{\mu}) = - \sin(k_{\mu}) = - v_{\mu}(k) \).
    Hence finally the Fourier transform of the overlap operator is
    \begin{equation}
        \tilde{D}_{k} = 1 + \dfrac{-i \sum_{\mu=1}^{4} \gamma^{\mu} \sin(k_{\mu}) + 2r \sum_{\mu=1}^{4}\sin^{2}(k_{\mu}/2) - M}{\left[\sum_{\mu=1}^{4}\sin^{2}(k_{\mu}) + \left(2r \sum_{\mu=1}^{4}\sin^{2}(k_{\mu}/2) - M\right)^{2}\right]^{1/2}}
    \end{equation}
    Let's finally compute di naive continuum limit
    \begin{align*}
      \lim_{a\to 0} \frac{\tilde{D}_{a p}}{a}
      &= \lim_{a\to 0}  \frac{1}{a} \left\{
        1 + \dfrac{-i \sum_{\mu=1}^{4} \gamma^{\mu} \sin(a p_{\mu}) + 2r \sum_{\mu=1}^{4}\sin^{2}(a p_{\mu}/2) - M}{\left[\sum_{\mu=1}^{4}\sin^{2}(a p_{\mu}) + \left(2r \sum_{\mu=1}^{4}\sin^{2}(a p_{\mu}/2) - M\right)^{2}\right]^{1/2}}\right\} \\
      &= \lim_{a \to 0} \frac{1}{a} \left(1 + \frac{-i a \cancel{p} - M}{M} + O(a^{2})\right) \\
      &= -\frac{i \cancel{p}}{M}
    \end{align*}
    which is what we wanted up to the overall \( M \) constant.

    \item The path integral for a gauge theory coupled to the dynamical fermions on a lattice has a partition function of the form
    \begin{equation}
        Z = \int [\dif{U_{\mu}(n)}][\dif{\bar{\psi}} \dif{\psi}] \, e^{-S_{W}[U]} \det\left(D_{ov} + \hat{m}\right)
    \end{equation}
    The overlap operator has no sign problem. Indeed since \( D_{ov} \) is a \( \gamma_{5} \)-hermitian operator
    \begin{equation}
        (D_{ov} \gamma_{5})^{\dagger} = \gamma_{5} D_{ov}^{\dagger} = \gamma_{5}\left(1 + \frac{H}{\sqrt{H^{\dagger} H}} \gamma_{5}\right) = D_{ov} \gamma_{5}
    \end{equation}
    its eigenvalues comes in complex-conjugate pairs: indeed if \( D_{ov} u_{\lambda} = \lambda u_{\lambda} \) then
    \begin{equation}
        D_{ov} \gamma_{5} u_{\lambda} = \gamma_{5} D_{ov}^{\dagger} u_{\lambda} = \lambda^{*} \gamma_{5}u_{\lambda}
    \end{equation}
    hence \( \gamma_{5} u_{\lambda} \) is an eigenvector with eigenvalue \( \lambda^{*} \). Hence the determinant will be
    \begin{align*}
      \det\left(D_{ov} + \hat{m}\right) &= \prod_{\lambda \in \sigma \cap \R}(\lambda + \hat{m}) \prod_{\lambda \in \sigma \cap \Im(\lambda) > 0} (\lambda + \hat{m})(\lambda^{*} + \hat{m}) \\
                                        &= \prod_{\lambda \in \sigma \cap \R}(\lambda + \hat{m}) \prod_{\lambda \in \sigma \cap \Im(\lambda) > 0} (\,\abs{\lambda}^{2} + \hat{m}^{2})
    \end{align*}
    hence we might have a sign problem if there were negative enough real eigenvalues. However the Ginsparg-Wilson relation disallow such scenario: using the \( \gamma_{5} \)-hermiticity we have
    \begin{gather*}
        \gamma_{5}\left(D_{ov}\gamma_{5} + \gamma_{5} D_{ov}\right) = \gamma_{5} D_{ov} \gamma_{5} D_{ov} \\
        D_{ov}^{\dagger} + D_{ov} = D_{ov}^{\dagger} D_{ov}
    \end{gather*}
    contracting with an eigenvector \( u_{\lambda} \)
    \begin{gather*}
        u_{\lambda}^{\dagger}(D_{ov}^{\dagger} + D_{ov}) u_{\lambda} = u_{\lambda}^{\dagger} D_{ov}^{\dagger} D_{ov} u_{\lambda} \\
        \lambda^{*} + \lambda = \abs{\lambda}^{2}
    \end{gather*}
    Writing \( \lambda = x + i y \) we finally find that the eigenvalues must lay on a circumference
    \begin{equation}
        (x-1)^{2} + y^{2} = 0
    \end{equation}
    hence the real eigenvalues are positive (\( \lambda = 0, 2 \)) and there is no sign problem.
\end{enumerate}

\section{CFT and anomalies: problem 2}
\begin{enumerate}[label=(\alph*)]
    \item Since the theory is free we compute correlation functions via the Wick's theorem, reducing the \( n \)-point functions to product of 2-point functions coming from all possible contractions
    \begin{equation}
        \braket{\phi(x_{1})\phi(y_{2})} = \frac{1}{x_{12}^{2\Delta}} = \frac{1}{x_{12}} \qquad x_{ij} = \abs{x_{i} - x_{j}}
    \end{equation}
    since \( \Delta = (d-2)/2 = 1/2 \) in 3 dimensions. The 4-point function of \( O(x) = \norder{\phi^{2}(x)} / \sqrt{2} \) is then
    \begin{align*}
      &\braket{O(x_1)O(x_{2})O(x_{3})O(x_{4})}
      = \frac{1}{4} \braket{\norder{\phi^{2}(x_{1})}  \norder{\phi^{2}(x_{2})}  \norder{\phi^{2}(x_{3})}  \norder{\phi^{2}(x_{4})}} \\
      &\qquad = \frac{1}{4} 4\left(
        \frac{1}{x_{12}^{2\Delta}} \frac{1}{x_{12}^{2\Delta}} \frac{1}{x_{34}^{2\Delta}} \frac{1}{x_{34}^{2\Delta}}
        + \frac{1}{x_{12}^{2\Delta}} \frac{1}{x_{13}^{2\Delta}} \frac{1}{x_{24}^{2\Delta}} \frac{1}{x_{34}^{2\Delta}}
        + \frac{1}{x_{12}^{2\Delta}} \frac{1}{x_{14}^{2\Delta}} \frac{1}{x_{23}^{2\Delta}} \frac{1}{x_{34}^{2\Delta}} \right. \\
      &\qquad  \qquad\qquad \left.
        + \frac{1}{x_{13}^{2\Delta}} \frac{1}{x_{13}^{2\Delta}} \frac{1}{x_{24}^{2\Delta}} \frac{1}{x_{24}^{2\Delta}}
        + \frac{1}{x_{13}^{2\Delta}} \frac{1}{x_{14}^{2\Delta}} \frac{1}{x_{23}^{2\Delta}} \frac{1}{x_{24}^{2\Delta}}
        + \frac{1}{x_{14}^{2\Delta}} \frac{1}{x_{14}^{2\Delta}} \frac{1}{x_{24}^{2\Delta}} \frac{1}{x_{24}^{2\Delta}}
        \right) \\
      &\qquad = \frac{1}{x_{12}^{4\Delta} x_{34}^{4\Delta}}\left[
        1 + \frac{x_{12}^{2\Delta} x_{34}^{2\Delta}}{x_{13}^{2\Delta}x_{24}^{2\Delta}}
        + \frac{x_{12}^{2\Delta} x_{34}^{2\Delta}}{x_{14}^{2\Delta} x_{23}^{2\Delta}}
        + \frac{x_{12}^{4\Delta} x_{34}^{4\Delta}}{x_{13}^{4\Delta} x_{24}^{4\Delta}}
        + \frac{x_{12}^{4\Delta} x_{34}^{4\Delta}}{x_{13}^{2\Delta} x_{14}^{2\Delta} x_{23}^{2\Delta} x_{24}^{2\Delta}}
        + \frac{x_{12}^{4\Delta} x_{34}^{4\Delta}}{x_{14}^{4\Delta}x_{24}^{4\Delta}}
        \right]
    \end{align*}
    Using the conformal invariants
    \begin{equation}
        u = \frac{x_{12}^{2}x_{34}^{2}}{x_{13}^{2} x_{24}^{2}} \qquad v = \frac{x_{14}^{2}x_{23}^{2}}{x_{13}^{2}x_{24}^{2}}
    \end{equation}
    it becomes
    \begin{equation}
        \braket{O(x_1)O(x_{2})O(x_{3})O(x_{4})} = \frac{1}{x_{12}^{4\Delta} x_{34}^{4\Delta}}\left[1 + u^{\Delta} + \left(\frac{u}{v}\right)^{\Delta} + u^{2\Delta} + \frac{u^{2\Delta}}{v^{\Delta}} + \left(\frac{u}{v}\right)^{2\Delta}\right]
    \end{equation}
    This has indeed the form dictated by conformal symmetry since it's in the form
    \begin{equation}
        \braket{O(x_1)O(x_{2})O(x_{3})O(x_{4})} = \frac{1}{x_{12}^{2\Delta_{O}} x_{34}^{2\Delta_{O}}} g(u, v)
    \end{equation}
    where \( \Delta_{O} = 2 \Delta \) since the theory is free and \( g(u, v) \) is a scalar function of the conformal invariants
    \begin{equation}
        g(u, v) = 1 + u^{\Delta} + \left(\frac{u}{v}\right)^{\Delta} + u^{2\Delta} + \frac{u^{2\Delta}}{v^{\Delta}} + \left(\frac{u}{v}\right)^{2\Delta}
    \end{equation}
    The crossing equation is satisfied
    \begin{align*}
      \left(\frac{u}{v}\right)^{\Delta_{O}}g(v, u)
      &= \left(\frac{u}{v}\right)^{2\Delta} \left[1 + v^{\Delta} + \left(\frac{v}{u}\right)^{\Delta} + v^{2\Delta} + \frac{v^{2\Delta}}{u^{\Delta}} + \left(\frac{v}{u}\right)^{2\Delta}\right] \\
      &= \left(\frac{u}{v}\right)^{2\Delta}  + \frac{u^{2\Delta}}{v^{\Delta}} + \left(\frac{u}{v}\right)^{\Delta} + u^{2\Delta} + u^{\Delta} + 1 \\
      &= g(u, v)
    \end{align*}

    \item The conformal block decomposition is
    \begin{equation}
        g(u, v) = \sum_{\Delta, \ell} \lambda_{\Delta, \ell}^{2} \, g_{\Delta, \ell}(u, v)
    \end{equation}
    where the sum runs over the quantum numbers \( (\Delta, \ell) \) of the primary operators appearing in the OPE \( O \times O \). Since the theory is free we can compute the OPE using again Wick's theorem. Indeed
    \begin{align*}
      O(0) \times O(x)
      &= \frac{1}{2} \norder{\phi^{2}(0)} \times \norder{\phi^{2}(x)} \\
      &= \frac{1}{2} \norder{\phi^{2}(0) \phi^{2}(x)} +\frac{1}{2} 2\braket{\phi(0) \phi(x)} \norder{\phi(0)\phi(x)} + \frac{1}{2} 2\braket{\phi(0) \phi(x)} ^{2} \\
      &= \frac{1}{x^{4}} + \frac{1}{x^{2}} \sum_{\ell\geq 0} \frac{1}{k_{1}! \cdots k_{\ell}!} x^{\mu_{1}} \cdots x^{\mu_{\ell}} \norder{\phi(0) \partial_{\mu_{1}} \cdots \partial_{\mu_{\ell}} \phi(0)} \\
      & \quad + \sum_{\ell\geq 0} \frac{1}{k_{1}! \cdots k_{\ell}!} x^{\mu_{1}} \cdots x^{\mu_{\ell}} \norder{\phi^{2}(0) \partial_{\mu_{1}} \cdots \partial_{\mu_{\ell}} \phi^{2}(0)}
    \end{align*}
    From this expression we understand that the OPE will contain the identity operator, operators with dimensions \( 2\Delta + \ell = 1 + \ell \) and \( 4\Delta + \ell = 2 + \ell \) for \( \ell \in \N \) (\( D = 3 \)).

    Therefore the conformal block decomposition will take the form
    \begin{equation}
        g(u, v) = 1 + \sum_{\ell=0}^{+\infty} a_{\ell} g_{2\Delta+\ell, \ell}(u, v) + \sum_{\ell=0}^{+\infty} b_{\ell} g_{4\Delta+\ell, \ell}(u, v)
    \end{equation}
    where \( a_{\ell} = \lambda^{2}_{2\Delta+ \ell} \) and \( b_{\ell} = \lambda^{2}_{4\Delta+\ell,\ell} \).

    In \( D = 3 \), we expect \( a_{2} = \lambda_{3,2} \neq 0 \) since this corresponds to the OPE coefficient of the stress tensor \( \lambda_{TOO} \) which should not vanish by consistency with the associated Ward identity.
    \item For concreteness we specialize to \( D = 3 \Rightarrow \Delta = 1/2 \). The conformal block expansion is
    \begin{equation}
        g(u, v) = 1 + \sum_{\ell=0}^{+\infty} a_{\ell} g_{1+\ell, \ell}(u, v) + \sum_{\ell=0}^{+\infty} b_{\ell} g_{2+\ell, \ell}(u, v)
    \end{equation}
    If the conformal block \( g_{\Delta, \ell} \) starts with a power \( r^{\Delta} \) times a power series in \( r \)
    \begin{equation}
        g_{\Delta, \ell}(r, \eta) = r^{\Delta}\sum_{k=0}^{+\infty} \alpha_{\Delta, \ell}^{(k)}(\eta) r^{k}
    \end{equation}
    then
    \begin{align*}
      g(r, \eta)
      &= 1 + \sum_{\ell=0}^{+\infty} \left(a_{\ell} r^{1+\ell}\sum_{k=0}^{+\infty}\alpha_{1+\ell, \ell}^{(k)}(\eta) r^{k} +  b_{\ell} r^{2+\ell}\sum_{k=0}^{+\infty}\alpha_{2+\ell, \ell}^{(k)}(\eta) r^{k}\right) \\
      &= 1 +  \sum_{\ell=0}^{+\infty} r^{1+\ell} \left(a_{\ell} \sum_{k=0}^{+\infty}\alpha_{1+\ell, \ell}^{(k)}(\eta) r^{k} +  b_{\ell} \sum_{k=0}^{+\infty}\alpha_{2+\ell, \ell}^{(k)}(\eta) r^{k+1}\right) \\
      &= 1 + \sum_{\ell, k=0}^{+\infty} \left(a_{\ell} \alpha_{1+\ell, \ell}^{(k)}(\eta)  +  b_{\ell} \alpha_{2+\ell, \ell}^{(k-1)}(\eta)\right) r^{1+\ell+k}
    \end{align*}
    with the convetion that \( \alpha^{(-1)}_{\Delta, \ell} = 0 \).
    We now consider the limit \( r \to 0 \) and keep terms only up to \( O(r^{4}) \) which amounts to \( \ell + k \leq 2 \)
    \begin{align*}
      g(u, v)
      &=
        1
        + a_{0}\alpha_{1,0}^{(0)} r
        + \left(a_{0}\alpha_{1,0}^{(1)} + b_{0} \alpha_{2,0}^{(0)} + a_{1}\alpha_{2,1}^{(0)}\right)r^{2} \\
      &\quad + \left(a_{0}\alpha_{1,0}^{(2)} + b_{0}\alpha_{2,0}^{(1)} + a_{1}\alpha_{2,1}^{(1)}+b_{1}\alpha_{3,1}^{(0)} + a_{2}\alpha_{3,2}^{(0)} \right)r^{3}  + O(r^{4}) \\
      &= 1 + a_{0}r(\alpha_{1,0}^{(0)} + \alpha_{1,0}^{(1)} r + \alpha_{1,0}^{(2)} r^{2})
        + a_{1}  r^{2}(\alpha_{2,1}^{(0)} + \alpha_{2,1}^{(1)}r) + a_{2} \alpha_{3,2}^{(0)} r^{3}\\
      &\quad + b_{0} r^{2}(\alpha_{2,0}^{(0)} + \alpha_{2,0}^{(1)} r) + b_{1} r^{3} \alpha_{3,1}^{(0)} + O(r^{4}) \\
      &= 1 + a_{0} g_{1,0}(r, \eta) + a_{1} g_{2,1}(r,\eta) + a_{2}g_{3,2}(r,\eta) + b_{0} g_{2,0}(r, \eta) + b_{1} g_{3,1}(r,\eta) + O(r^{4})
    \end{align*}
    Using the given expressions for the conformal blocks we find
    \begin{align*}
      g(r, \eta)
      &= 1+a_0 r+r^2 \left(b_0-a_1 \eta \right)+r^3 \left(a_0 \eta ^2+\frac{1}{2} a_2 (3 \eta ^2-1)-b_1 \eta \right)+O(r^{4})
    \end{align*}
    On the other hand we have the exact expression for \( g(u, v) \), hence we can plug in the expressions for \( u, v \) in terms of \( (r, \eta) \), expand to \( O(r^{4}) \) the expression. This gives
    \begin{align}
      g(r, \eta) = 1+8 r+48 r^2+(32 \eta ^2-8) r^3+O(r^4)
    \end{align}
    Matching the coefficients we find
    \begin{equation}
        a_{0} = 8 \qquad
        a_{1} = 0 \qquad
        a_{2} = 16 \qquad
        b_{0} = 48 \qquad
        b_{1} = 0
    \end{equation}
    From these results we can make the following statements
    \begin{itemize}
        \item the square of the computed OPE coefficents are positive as expected from unitarity
        \item there is no contribution from the \( \ell = 1 \) sector since they could only come from \( g_{2,1} \) and \( g_{3,1} \) whose OPE coefficients \( a_{1}, b_{1} \) are zero. Indeed looking at the OPE expansion those operators are descendants
        \begin{equation}
            V_{\mu} \sim \norder{\phi(0) \partial_{\mu} \phi(0)} = \frac{1}{2}\partial_{\mu}\norder{\phi^{2}(0)}
            \qquad
            W_{\mu} \sim \norder{\phi^{2}(0) \partial_{\mu} \phi^{2}(0)} = \frac{1}{2}\partial_{\mu}\norder{\phi^{4}(0)}
        \end{equation}
        and the sum in the conformal block decomposition runs only over primaries in the OPE.
        \item the stress tensor should be an operator with \( (\Delta, \ell) = (3, 2) \), its OPE coefficient is then
        \begin{equation}
            \abs{\lambda_{O O T_{\mu\nu}}} = \abs{\lambda_{3,2}} = \sqrt{a_{2}} = 4
        \end{equation}
        \item the remaining OPE coefficients that we have found are associated to the primary operators
        \begin{align*}
          \lambda_{1,0}^{2} = a_{0}
          \quad \rightarrow \quad O_{1,0} &= O(0)= \frac{1}{\sqrt{2}} \norder{\phi^{2}(0)} \\
          \lambda_{2,0}^{2} = b_{0}
          \quad \rightarrow \quad O_{2,0} &= \frac{1}{\sqrt{4!}} \norder{\phi^{4}(0)}
        \end{align*}
    \end{itemize}
\end{enumerate}
\end{document}